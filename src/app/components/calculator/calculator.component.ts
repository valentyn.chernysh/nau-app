import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl, ValidatorFn } from '@angular/forms';

import { InputErrorStateMatcher } from './../../providers/inputErrorStateMatcher';

import { MatDialog, MatDialogRef, MatStepper } from '@angular/material';

import { AddHouseComponent } from './../add-house/add-house.component';
import { AddRoomComponent } from './../add-room/add-room.component';
import { AddJobTypeComponent } from './../add-jopType/add-jopType.component';
import { AddWorkerComponent } from './../add-worker/add-worker.component';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.less']
})

export class CalculatorComponent implements OnInit {

  @ViewChild('stepper') stepper: MatStepper;

  public step2FormGroup: FormGroup;

  public step3FormGroup: FormGroup;

  public step5FormGroup: FormGroup;

  public inputErrorStateMatcher = new InputErrorStateMatcher();

  public isShowProgress = true;

  public houses = [];
  public currentHouse = '';

  public rooms = [];

  public choosenJobTypes = [];
  public jobTypes = [];

  public choosenMaterials = {};
  public meterials = {};

  public workers = [];

  public price = 0;

  constructor(private dialog: MatDialog, private dialogRef: MatDialogRef<CalculatorComponent>) { 
    if (localStorage.getItem('houses')) {
      this.houses = JSON.parse(localStorage.getItem('houses'));
    } else {
      this.houses = [
        'вул. Миколи Ушакова, 10',
        'вул. Миколи Ушакова, 12',
        'вул. Миколи Ушакова, 14',
      ]; 
    }
    this.currentHouse = this.houses[0];

    if (localStorage.getItem('rooms')) {
      this.rooms = JSON.parse(localStorage.getItem('rooms'));
    } else {
      this.rooms = [
        'Однокімнатні',
        'Двохкімнатні',
        'Трьохкімнатні'
      ]; 
    }

    if (localStorage.getItem('jobTypes')) {
      this.jobTypes = JSON.parse(localStorage.getItem('jobTypes'));
    } else {
      this.jobTypes = [
        'Грунтування стін',
        'Проклеювання шпалер',
        'Застелення підлоги ламінатом',
        'Встановлення євро-вікон'
      ]; 
    }

    if (localStorage.getItem('meterials')) {
      this.meterials = JSON.parse(localStorage.getItem('meterials'));
    } else {
      this.meterials = {
        'Грунтування стін': [
          'Грунтівка 1',
          'Грунтівка 2',
          'Грунтівка 3'
        ],
        'Проклеювання шпалер': [
          'Червоні шпалери',
          'Сині шпалери',
          'Зелені шпалери'
        ],
        'Застелення підлоги ламінатом': [
          'Ламінат з дерева',
          'Ламінат з пластику',
          'Ламінат з граніту'
        ],
        'Занесення базового набору меблів': [
          'Шафа',
          'Ліжко',
          'Стіл'
        ],
        'Обладнання ванної кімнати': [
          'Туалет',
          'Душ',
          'Раковина'
        ],
        'Обладнання кухні': [
          'Плита',
          'Холодильник',
          'Піч'
        ],
        'Встановлення євро-вікон': [
          'Євро-вікна 1',
          'Євро-вікна 2',
          'Євро-вікна 3'
        ]
      } 
    }

    if (localStorage.getItem('workers')) {
      this.workers = JSON.parse(localStorage.getItem('workers'));
    } else {
      this.workers = [
        'Вася',
        'Петя',
        'Женя',
        'Саша',
        'Леша',
        'Дима'
      ]; 
    } 
  }

  ngOnInit() {

    this.stepper.animationDone.subscribe(() => {
      console.log(this.stepper.selectedIndex);
      if (this.stepper.selectedIndex === 4) {
        setTimeout(() => {
          this.price = Math.floor((Math.random() * 10000) + 1000);
          this.isShowProgress = false;
        }, 5000);
      }
    });

    this.step2FormGroup = new FormGroup({
      'totalCheck': new FormControl('', [
        Validators.required
      ])
    });
    for (let i = 0; i < this.rooms.length; i++) {
      this.step2FormGroup.addControl('check' + (i + 1), new FormControl(''));
      this.step2FormGroup.addControl('count' + (i + 1), new FormControl(''));
    }

    this.step3FormGroup = new FormGroup({
      'totalCheck': new FormControl('', [
        Validators.required
      ])
    });
    for (let i = 0; i < this.jobTypes.length; i++) {
      this.step3FormGroup.addControl('check' + (i + 1), new FormControl(''));
    }

    this.step5FormGroup = new FormGroup({
      'totalCheck': new FormControl('', [
        Validators.required
      ])
    });
    for (let i = 0; i < this.workers.length; i++) {
      this.step5FormGroup.addControl('check' + (i + 1), new FormControl(''));
    }

  }

  // Step 1
  public addHouse = () => {
    const dialogRef = this.dialog.open(AddHouseComponent, {});
    dialogRef.afterClosed().subscribe(msg => {
      if (msg) {
        this.houses.push(msg);
        this.currentHouse = msg; 
        localStorage.setItem('houses', JSON.stringify(this.houses));
      }
    });
  }

  // Step 2
  public addRoom = () => {
    const dialogRef = this.dialog.open(AddRoomComponent, {});
    dialogRef.afterClosed().subscribe(msg => {
      if (msg) {
        this.rooms.push(msg);
        this.step2FormGroup.addControl('check' + (this.rooms.length), new FormControl(''));
        this.step2FormGroup.addControl('count' + (this.rooms.length), new FormControl(''));
        localStorage.setItem('rooms', JSON.stringify(this.rooms));
      }
    });
  }

  public onStep2CheckChange = (index) => {
    let isOk = false;

    for (let i = 0; i < this.rooms.length; i++) {
      if (this.step2FormGroup.get('check' + (i + 1)).value) {
        isOk = true;
      }
    }

    if (isOk) {
      this.step2FormGroup.get('totalCheck').setValue('true');
    } else {
      this.step2FormGroup.get('totalCheck').setValue('');
    }

    if (this.step2FormGroup.get('check' + index).value) {
      this.step2FormGroup.get('count' + index).setValidators([Validators.required]);
      this.step2FormGroup.get('count' + index).setValue(1);
    } else {
      this.step2FormGroup.get('count' + index).setValue('');
      this.step2FormGroup.get('count' + index).clearValidators();
      this.step2FormGroup.get('count' + index).reset();
    }
  }

  // Step 3
  public addJobType = () => {
    const dialogRef = this.dialog.open(AddJobTypeComponent, {});
    dialogRef.afterClosed().subscribe(msg => {
      if (msg) {
        this.jobTypes.push(msg);
        this.step3FormGroup.addControl('check' + (this.jobTypes.length), new FormControl(''));
        localStorage.setItem('jobTypes', JSON.stringify(this.jobTypes));
      }
    });
  }

  public onStep3CheckChange = () => {
    let isOk = false;

    for (let i = 0; i < this.jobTypes.length; i++) {
      if (this.step3FormGroup.get('check' + (i + 1)).value && this.choosenJobTypes.indexOf(this.jobTypes[i]) === -1) {
        isOk = true;
        this.choosenJobTypes.push(this.jobTypes[i]);
        this.choosenMaterials[this.jobTypes[i]] = this.meterials[this.jobTypes[i]][0];
      }
    }

    if (isOk) {
      this.step3FormGroup.get('totalCheck').setValue('true');
    } else {
      this.step3FormGroup.get('totalCheck').setValue('');
    }
  }

  // Step 5
  public onStep5CheckChange = () => {
    let isOk = false;

    for (let i = 0; i < this.workers.length; i++) {
      if (this.step5FormGroup.get('check' + (i + 1)).value) {
        isOk = true;
      }
    }

    if (isOk) {
      this.step5FormGroup.get('totalCheck').setValue('true');
    } else {
      this.step5FormGroup.get('totalCheck').setValue('');
    }
  }

  public addWorker = () => {
    const dialogRef = this.dialog.open(AddWorkerComponent, {});
    dialogRef.afterClosed().subscribe(msg => {
      if (msg) {
        this.workers.push(msg);
        this.step5FormGroup.addControl('check' + (this.workers.length), new FormControl(''));
        localStorage.setItem('workers', JSON.stringify(this.workers));
      }
    });
  }

  // Close
  public close = () => {
    this.dialogRef.close();
  }

}
