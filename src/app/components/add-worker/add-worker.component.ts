import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { InputErrorStateMatcher } from './../../providers/inputErrorStateMatcher';

import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-worker',
  templateUrl: './add-worker.component.html',
  styleUrls: ['./add-worker.component.less']
})

export class AddWorkerComponent implements OnInit {

  public workerForm: FormGroup;

  public inputErrorStateMatcher = new InputErrorStateMatcher();

  constructor(private dialogRef: MatDialogRef<AddWorkerComponent>) { }

  ngOnInit() {
    this.workerForm = new FormGroup({
      'worker': new FormControl('', [
        Validators.required
      ])
    });
  }

  public close = () => {
    this.dialogRef.close();
  }

  public submit = () => {
    if (this.workerForm.valid) {
      this.dialogRef.close(this.workerForm.get('worker').value);
    }
  }

}
