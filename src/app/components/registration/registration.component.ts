import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { InputErrorStateMatcher } from './../../providers/inputErrorStateMatcher';

import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.less']
})
export class RegistrationComponent implements OnInit {

  public registrationForm: FormGroup;

  public inputErrorStateMatcher = new InputErrorStateMatcher();

  constructor(private dialogRef: MatDialogRef<RegistrationComponent>) { }

  ngOnInit() {
    this.registrationForm = new FormGroup({
      'name': new FormControl('', [
        Validators.required
      ]),
      'lastname': new FormControl('', [
        Validators.required
      ]),
      'email': new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      'password': new FormControl('', [
        Validators.required
      ]),
    });
  }

  public showAuthorizationForm = () => {
    this.dialogRef.close('show_authorization_form');
  }

  public registration = () => {
    if (this.registrationForm.valid) {
      let logins = JSON.parse(localStorage.getItem('logins'));

      logins[this.registrationForm.get('email').value] = {
        'login': this.registrationForm.get('email').value,
        'password': this.registrationForm.get('password').value
      }

      localStorage.setItem('logins', JSON.stringify(logins));

      this.dialogRef.close(this.registrationForm.get('email').value);
    }
  }

}
