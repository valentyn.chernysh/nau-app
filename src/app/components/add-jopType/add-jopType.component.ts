import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { InputErrorStateMatcher } from './../../providers/inputErrorStateMatcher';

import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-jopType',
  templateUrl: './add-jopType.component.html',
  styleUrls: ['./add-jopType.component.less']
})

export class AddJobTypeComponent implements OnInit {

  public jobTypeForm: FormGroup;

  public inputErrorStateMatcher = new InputErrorStateMatcher();

  constructor(private dialogRef: MatDialogRef<AddJobTypeComponent>) { }

  ngOnInit() {
    this.jobTypeForm = new FormGroup({
      'jobType': new FormControl('', [
        Validators.required
      ])
    });
  }

  public close = () => {
    this.dialogRef.close();
  }

  public submit = () => {
    if (this.jobTypeForm.valid) {
      this.dialogRef.close(this.jobTypeForm.get('jobType').value);
    }
  }

}
