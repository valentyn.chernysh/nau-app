import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { InputErrorStateMatcher } from './../../providers/inputErrorStateMatcher';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  public inputErrorStateMatcher = new InputErrorStateMatcher();

  constructor(private dialogRef: MatDialogRef<LoginComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      'email': new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      'password': new FormControl('', [
        Validators.required
      ])
    });

    if (this.data && this.data['email'] && this.data['email'].length > 0) {
      this.loginForm.get('email').setValue(this.data['email']);
    }
  }

  public showRegistrationForm = () => {
    this.dialogRef.close('show_registration_form');
  }

  public login = () => {
    if (this.loginForm.valid) {
      let logins = JSON.parse(localStorage.getItem('logins'));

      if (
        logins[this.loginForm.get('email').value] &&
        logins[this.loginForm.get('email').value]['login'] == this.loginForm.get('email').value &&
        logins[this.loginForm.get('email').value]['password'] == this.loginForm.get('password').value
      ) {
        localStorage.setItem('isAuthorized', 'true');
        this.dialogRef.close('ok');
      }

    }
  }

}
