import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { InputErrorStateMatcher } from './../../providers/inputErrorStateMatcher';

import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-house',
  templateUrl: './add-house.component.html',
  styleUrls: ['./add-house.component.less']
})

export class AddHouseComponent implements OnInit {

  public houseForm: FormGroup;

  public inputErrorStateMatcher = new InputErrorStateMatcher();

  constructor(private dialogRef: MatDialogRef<AddHouseComponent>) { }

  ngOnInit() {
    this.houseForm = new FormGroup({
      'house': new FormControl('', [
        Validators.required
      ])
    });
  }

  public close = () => {
    this.dialogRef.close();
  }

  public submit = () => {
    if (this.houseForm.valid) {
      this.dialogRef.close(this.houseForm.get('house').value);
    }
  }

}
