import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { InputErrorStateMatcher } from './../../providers/inputErrorStateMatcher';

import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-room',
  templateUrl: './add-room.component.html',
  styleUrls: ['./add-room.component.less']
})

export class AddRoomComponent implements OnInit {

  public roomForm: FormGroup;

  public inputErrorStateMatcher = new InputErrorStateMatcher();

  constructor(private dialogRef: MatDialogRef<AddRoomComponent>) { }

  ngOnInit() {
    this.roomForm = new FormGroup({
      'room': new FormControl('', [
        Validators.required
      ])
    });
  }

  public close = () => {
    this.dialogRef.close();
  }

  public submit = () => {
    if (this.roomForm.valid) {
      this.dialogRef.close(this.roomForm.get('room').value);
    }
  }

}
