import { Component } from '@angular/core';

import { ModalsService } from './services/modals.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})

export class AppComponent {

  constructor(private modalsService: ModalsService) {
    if (localStorage.getItem('isAuthorized')) {
      this.modalsService.openCalculatorDialog();
    } else {
      if (!localStorage.getItem('logins')) {
        localStorage.setItem('logins', JSON.stringify({
          'admin': {
            'login': 'admin@admin.com',
            'password': '1234'
          }
        }))
      }
      this.modalsService.openAuthorizationDialog();
    }
  }

}
