import './../polyfills';

import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from './material.module';

import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { CalculatorComponent } from './components/calculator/calculator.component';
import { AddHouseComponent } from './components/add-house/add-house.component';
import { AddRoomComponent } from './components/add-room/add-room.component';
import { AddJobTypeComponent } from './components/add-jopType/add-jopType.component';
import { AddWorkerComponent } from './components/add-worker/add-worker.component';

import { ModalsService } from './services/modals.service';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    LoginComponent,
    RegistrationComponent,
    CalculatorComponent,
    AddHouseComponent,
    AddRoomComponent,
    AddJobTypeComponent,
    AddWorkerComponent,

    AppComponent
  ],
  entryComponents: [
    LoginComponent,
    RegistrationComponent,
    CalculatorComponent,
    AddHouseComponent,
    AddRoomComponent,
    AddJobTypeComponent,
    AddWorkerComponent,

    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    ModalsService
  ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule { }
