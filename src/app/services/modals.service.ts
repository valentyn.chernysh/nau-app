import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';

import { LoginComponent } from './../components/login/login.component';
import { RegistrationComponent } from './../components/registration/registration.component';
import { CalculatorComponent } from './../components/calculator/calculator.component';

@Injectable()
export class ModalsService {

  constructor(private dialog: MatDialog) {}

  public openAuthorizationDialog(email?): void {
    const dialogRef = this.dialog.open(LoginComponent, { disableClose: true, data: {'email': email ? email : ''} });
    dialogRef.afterClosed().subscribe(msg => {
      if (msg && msg === 'show_registration_form') {
        this.openRegistrationDialog();
      }
      if (msg && msg === 'ok') {
        this.openCalculatorDialog();
      }
    });
  }

  public openRegistrationDialog(): void {
    const dialogRef = this.dialog.open(RegistrationComponent, { disableClose: true });
    dialogRef.afterClosed().subscribe(msg => {
      if (msg && msg === 'show_authorization_form') {
        this.openAuthorizationDialog();
      }
      if (msg && msg !== 'show_authorization_form') {
        this.openAuthorizationDialog(msg);
      }
    });
  }

  public openCalculatorDialog(): void {
    const dialogRef = this.dialog.open(CalculatorComponent, { disableClose: true });
    // dialogRef.afterClosed().subscribe(msg => {
    //   if (msg && msg === 'show_authorization_form') {
    //     this.openAuthorizationDialog();
    //   }
    // });
  }

}
